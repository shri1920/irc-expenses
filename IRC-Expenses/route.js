/*globals require, exports, console */
/*jslint nomen:true*/

var cradle = require("cradle"),
    db     = new (cradle.Connection)().database("irc-expenses"),
    _      = require("underscore");

exports.health = function (req, res) {
    "use strict";
    console.log("Application live from " + new Date());
    res.status(200).json({"msg": "ok"});
};

exports.recordExpenses = function (req, res) {
    // Function to Record the monthly expenses
    "use strict";
    req.body = req.body || {};
    // Read the req parameters
    var amountPaid  = req.body.amountPaid,
        paidBy      = req.body.paidBy,
        addedBy     = req.body.addedBy,
        month       = req.body.month,
        comment     = req.body.comment,
        info        = {},
        monthName   = {"jan": "01", "feb": "02", "mar": "03"};
    if (!amountPaid || !paidBy || !addedBy || !month || !comment) {
        res.status(400).json({msg: "missing required parameter", code: "BAD_REQUEST"});
        return;
    }
    console.log("[recordExpenses] [" + new Date().toISOString() + "] Request received to record expenses by " + addedBy);
    info = {
        "transaction": {
            "amountPaid": amountPaid,
            "paidBy": paidBy,
            "addedBy": addedBy
        },
        "comment": comment,
        "date": {
            "creationdate": new Date().toISOString()
        },
        "groupId": "01" + ":" + monthName[month] + ":" + new Date().getFullYear(),
        "type": "App.Bundle.Expenses"
    };
    // To record the transaction in DB
    db.save(info, function (err, data) {
        if (err) {
            console.log("[recordExpenses] [" + new Date().toISOString() + "] Error recording expenses");
            res.status(500).json({msg: "error recording expenses", code: "INTERNAL_ERROR"});
            return;
        }
        res.status(200).json({msg: "ok", res: data});
    });
};

exports.recordLoan = function (req, res) {
    // Function to Record the monthly expenses
    "use strict";
    req.body = req.body || {};
    // Read the req parameters
    var amountPaid  = req.body.amountPaid,
        paidBy      = req.body.paidBy,
        paidTo      = req.body.paidTo,
        addedBy     = req.body.addedBy,
        month       = req.body.month,
        comment     = req.body.comment,
        info        = {},
        monthName   = {"jan": "01", "feb": "02", "mar": "03"};
    if (!amountPaid || !paidBy || !paidTo || !addedBy || !month || !comment) {
        res.status(400).json({msg: "missing required parameter", code: "BAD_REQUEST"});
        return;
    }
    console.log("[recordLoan] [" + new Date().toISOString() + "] Request received to record loan by " + addedBy);
    info = {
        "transaction": {
            "amountPaid": amountPaid,
            "paidBy": paidBy,
            "paidTo": paidTo,
            "addedBy": addedBy
        },
        "comment": comment,
        "date": {
            "creationdate": new Date().toISOString()
        },
        "groupId": "01" + ":" + monthName[month] + ":" + new Date().getFullYear(),
        "type": "App.Bundle.Loan"
    };
    // To record the transaction in DB
    db.save(info, function (err, data) {
        if (err) {
            console.log("[recordLoan] [" + new Date().toISOString() + "] Error recording loan");
            res.status(500).json({msg: "error recording loan", code: "INTERNAL_ERROR"});
            return;
        }
        res.status(200).json({msg: "ok", res: data});
    });
};

exports.calculateAmount = function (req, res) {
    // Function to calculate and destribute the amount
    "use strict";
    db.view("myDesignDoc/expenses", function (err, docs) {
        var i, j, totalExpendature = 0, amount, users, expendature = {}, spentUsers, notSpentUsers, to, payment = {};
        users = ["Kaushik", "Sameer", "Shrisha", "Shubham"];
        expendature.totalFrom = {};
        expendature.from = {};
        if (docs.length > 0) {
            for (i = 0; i < docs.length; i += 1) {
                amount = docs[i].value[0].amountPaid;
                amount = parseInt(amount, 10);
                totalExpendature += amount;
                expendature.totalFrom[docs[i].value[0].paidBy] = amount;
                expendature.from[docs[i].value[0].paidBy] = amount / users.length;
            }
        }
        spentUsers    = Object.keys(expendature.from);
        notSpentUsers = _.difference(users, spentUsers);
        /*users         = ["Kaushik", "Sameer", "Shrisha", "Shubham"];
        spentUsers    = ["Kaushik", "Sameer", "Shrisha"];
        notSpentUsers = ["Shubham"];*/
        payment.from = {};
        for (i = 0; i < users.length; i += 1) {
            if (notSpentUsers.indexOf(users[i]) >= 0) {
                payment.from[users[i]] = expendature.from;
                continue;
            }
            to = _.difference(spentUsers, [users[i]]);
            console.log(users[i], to);
            for (j = 0; j < to.length; j += 1) {
                if (expendature.from[users[i]] < expendature.from[to[j]]) {
                    if (!payment.from[users[i]]) {
                        payment.from[users[i]] = {};
                    }
                    payment.from[users[i]][to[j]] = expendature.from[to[j]] - expendature.from[users[i]];
                }
            }
        }
        expendature.total = totalExpendature;
        expendature.payment = payment;
        console.log(expendature);
        res.status(200).json({msg: "ok", expendature: expendature});
    });
};