/*globals require, process, console */

var express    = require("express"),
    app        = express(),
	bodyParser = require("body-parser"),
	router     = express.Router(),
    route      = require("./route.js");

// Body-parser (To parse the request body)
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/* 
    Add to avoid cross origin access.
    Access-Control-Allow-Origin is set to '*' so that server REST APIs are accessible for all the domains.
    By setting domain name to some value, the API access can be restricted to only the mentioned domain. 
    Eg, Access-Control-Allow-Origin: 'mywebsite.com'
*/
app.use(function (req, res, next) {
	"use strict";
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "content-type");
	next();
});

// Set the port no
app.set("port", process.env.PORT || 3000);

// Api to check the Health of the service
router.get("/health", route.health);
// Api to record the expenses
router.post("/expenses", route.recordExpenses);
// Api to record the Loan
router.post("/loan", route.recordLoan);
// Api to calculate and destribute the amount
router.get("/calculateamount", route.calculateAmount);

app.use("/", router);

// Start the service
app.listen(app.get("port"));
console.log("IRC-Expenses app Started @ " + new Date() + " Running on port no: " + app.get("port"));
